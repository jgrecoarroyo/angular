import { EdsPage } from './app.po';

describe('eds App', () => {
  let page: EdsPage;

  beforeEach(() => {
    page = new EdsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
